<?php

namespace Drupal\employee_fitness_rewards\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\employee_fitness_rewards\Math\DistanceMath;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class UserAdminFormBase.
 */
class UserAdminFormBase extends FormBase {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * Drupal\employee_fitness_rewards\Math\DistanceMath definition.
   *
   * @var \Drupal\employee_fitness_rewards\Math\DistanceMath
   */
  protected $distanceMath;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $fitnessRewardsConfig;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $rewardsEntityStorage;

  /**
   * Constructs a new UserRewardsForm object.
   */
  public function __construct(
    Connection $database,
    DistanceMath $distance_math,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->database = $database;
    $this->distanceMath = $distance_math;
    $this->fitnessRewardsConfig = $config_factory->get('employee_fitness_rewards.config');
    try {
      $this->rewardsEntityStorage = $entity_type_manager->getStorage('fitness_reward_entity');
    } catch (\Exception $e) {
      $this->logger('employee_fitness_rewards')
        ->error('Error retrieving entity storage for fitness_reward_entity');
    }
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\Core\Form\FormBase|\Drupal\employee_fitness_rewards\Form\UserAdminFormBase
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('employee_fitness_rewards.distance_math'),
      $container->get('config.factory'),
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_admin_form_base';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      $this->messenger()->addStatus($key . ': ' . $value);
    }
  }

  /**
   * Get all fitness reward config entities
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  protected function getRewardEntities() {
    return $this->rewardsEntityStorage->loadMultiple();
  }

  /**
   * Select all completed rewards concatenated into a single unique key
   * TRIM()+0 is a bit of a hack. It removes trailing zeros.
   *
   * @return array
   */
  protected function getCompletedUserRewards() {
    $query = $this->database->query("
      SELECT CONCAT(uid, '|', reward_id, '|', TRIM(distance)+0) 
      AS reward 
      FROM employee_fitness_rewards_rewards
    ");
    $results = $query->fetchAll();
    // fetchAllKeyed logs warning.
    // building array manually
    $array = [];
    foreach ($results as $obj) {
      $array[] = $obj->reward;
    }
    return $array;
  }

}
