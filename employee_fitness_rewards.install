<?php

/**
 * @file
 * Install, update and uninstall functions for the Employee
 * Fitness Rewards module.
 */

/**
 * Implements hook_schema().
 */
function employee_fitness_rewards_schema() {
  $schema['employee_fitness_rewards_distance'] = [
    'description' => 'Weekly log for tracked distance',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique ID.',
      ],
      'uid' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => "User's {users}.uid",
      ],
      'distance' => [
        'type' => 'numeric',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'precision' => 12,
        'scale' => 4,
        'description' => 'Distance in kilometers a user has logged for this week',
      ],
      'timestamp' => [
        'type' => 'varchar',
        'length' => 12,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Timestamp for the week the distance was logged',
      ],
    ],
    'primary key' => ['id'],
    'foreign keys' => [
      'uid' => ['users' => 'uid'],
    ],
  ];
  $schema['employee_fitness_rewards_rewards'] = [
    'description' => 'Weekly log for tracked distance',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique ID.',
      ],
      'uid' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => "User's {users}.uid",
      ],
      'reward_id' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => 0,
        'description' => "Machine name for the reward Config Entity",
      ],
      'date_received' => [
        'type' => 'varchar',
        'length' => 12,
        'not null' => TRUE,
        'default' => 0,
        'description' => "Timestamp for the date the reward was issued to a user",
      ],
      'distance' => [
        'type' => 'numeric',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'precision' => 12,
        'scale' => 4,
        'description' => 'Distance in kilometers a user logged to reach award.',
      ],
    ],
    'primary key' => ['id'],
    'foreign keys' => [
      'uid' => ['users' => 'uid'],
    ],
  ];
  return $schema;
}
