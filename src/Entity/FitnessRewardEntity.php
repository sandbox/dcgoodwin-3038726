<?php

namespace Drupal\employee_fitness_rewards\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Fitness Reward entity.
 *
 * @ConfigEntityType(
 *   id = "fitness_reward_entity",
 *   label = @Translation("Fitness Reward"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\employee_fitness_rewards\FitnessRewardEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\employee_fitness_rewards\Form\FitnessRewardEntityForm",
 *       "edit" = "Drupal\employee_fitness_rewards\Form\FitnessRewardEntityForm",
 *       "delete" = "Drupal\employee_fitness_rewards\Form\FitnessRewardEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\employee_fitness_rewards\FitnessRewardEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "fitness_reward_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" =
 *   "/admin/config/fitness-rewards/fitness_reward_entity/{fitness_reward_entity}",
 *     "add-form" = "/admin/config/fitness-rewards/fitness_reward_entity/add",
 *     "edit-form" =
 *   "/admin/config/fitness-rewards/fitness_reward_entity/{fitness_reward_entity}/edit",
 *     "delete-form" =
 *   "/admin/config/fitness-rewards/fitness_reward_entity/{fitness_reward_entity}/delete",
 *     "collection" = "/admin/config/fitness-rewards/fitness_reward_entity"
 *   }
 * )
 */
class FitnessRewardEntity extends ConfigEntityBase implements FitnessRewardEntityInterface {

  /**
   * The Take A Fitness Reward ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Take A Fitness Reward label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Fitness Reward description
   *
   * @var string
   */
  protected $description;

  /**
   * The distance required to earn the reward
   *
   * @var float
   */
  protected $distance;

  /**
   * Is this reward recurring
   *
   * @var boolean
   */
  protected $recurring;

  /**
   * @var \Drupal\employee_fitness_rewards\Math\DistanceMath
   */
  protected $distanceMath;


  /**
   * FitnessRewardEntity constructor.
   *
   * Setting distanceMath through global service call
   * Dependency injection in an entity is currently not possible in Drupal 8.5
   * See link for details: https://www.drupal.org/node/2913224
   *
   * @param array $values
   * @param $entity_type
   * @param \Drupal\employee_fitness_rewards\Math\DistanceMath $distanceMath
   */
  public function __construct(array $values, $entity_type) {
    //$this should ideally use dependency injection. See link above
    $this->distanceMath = \Drupal::service('employee_fitness_rewards.distance_math');
    parent::__construct($values, $entity_type);
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * Get the value for distance.
   * Use distance_math service to convert to miles if set
   * Use this to display values in form
   *
   *
   * @return float
   */
  public function getDistance() {
    return $this->distanceMath->getDistance($this->distance);
  }

  /**
   * Get the value for distance.
   * Do not convert based on unit_of_measurement setting
   * Use this getter for calculations before sending to form
   *
   * @return float
   */
  public function getRawDistance() {
    return (float) $this->distance;
  }

  /**
   * Set the value for distance
   * convert to kilometers if set to miles in config
   *
   * @param float $distance
   */
  public function setDistance($distance) {
    $this->distance = $this->distanceMath->getDistanceInsert($distance);
  }

  /**
   * @return bool
   */
  public function isRecurring() {
    return $this->recurring;
  }

  /**
   * @param bool $recurring
   */
  public function setRecurring($recurring) {
    $this->recurring = $recurring;
  }

}
