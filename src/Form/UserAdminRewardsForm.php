<?php

namespace Drupal\employee_fitness_rewards\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Class UserRewardsForm.
 */
class UserAdminRewardsForm extends UserAdminFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_rewards_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $header = [
      'username' => $this->t('Username'),
      'reward' => $this->t('Reward Title'),
      'distance' => $this->t('Distance (@unit)',
        ['@unit' => $this->fitnessRewardsConfig->get('unit_of_measurement')]),
      'recurring' => $this->t('Recurring'),
    ];

    $options = $this->getAvailableRewards();

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => $this->t('No records to display.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selected = $form_state->getValue('table');
    $rewards = [];
    $query = $this->database->insert('employee_fitness_rewards_rewards')
      ->fields(['uid', 'reward_id', 'distance', 'date_received']);
    foreach ($selected as $reward) {
      if (!empty($reward[0])) {
        $reward = explode('|', $reward);
        $reward = [
          'uid' => $reward[0],
          'reward_id' => $reward[1],
          'distance' => $reward[2],
          'date_received' => time(),
        ];
        $query->values($reward);
        $rewards[] = $reward;
      }
    }
    if (!empty($rewards)) {
      try {
        $query->execute();
        $plural = (count($rewards) == 1) ? 'record' : 'records';
        $this->messenger()->addStatus($this
          ->t('Successfully logged %count @plural.', [
            '%count' => count($rewards),
            '@plural' => $plural,
          ]));
      } catch (\Exception $e) {
        $this->logger('employee_fitness_rewards')
          ->error('Failed to insert fitness rewards into database');
      }
    }
    else {
      $this->messenger()->addWarning($this->t('No rewards selected.'));
    }
  }

  /**
   * @return array
   */
  private function getAvailableRewards() {
    $rewards = $this->getRewardEntities();
    $user_distance = $this->getUserTotalMiles();
    $completed_rewards = $this->getCompletedUserRewards();

    $available_rewards = [];
    //loop over each reward for each user
    foreach ($user_distance as $uid => $distance) {
      foreach ($rewards as $reward) {
        $recurring = $reward->isRecurring();

        //get the username for the user
        $account = User::load($uid);
        $username = $account->getUsername();

        $reward_distance = $reward->getRawDistance();

        //If reward is recurring, the check all possible
        //rewards the user may be eligible for
        if ($recurring) {
          if ($distance >= $reward_distance) {
            //use range to loop over all possible reward distances
            foreach (range($reward_distance, (
              floor($distance / $reward_distance) * $reward_distance),
              $reward_distance) as $possible_distance) {
              //building a unique identifier with uid, reward machine name, and distance
              $key = $uid . '|' . $reward->id() . '|' . trim($possible_distance);
              //check if current award has already been completed
              if (!in_array($key, $completed_rewards)) {
                $available_rewards[$key] = $this->formatReward($username,
                  $reward->label(),
                  $possible_distance,
                  $recurring);
              }
            }
          }
        }
        //if not recurring, only need to check single value
        else {
          if ($distance >= $reward_distance) {
            $key = $uid . '|' . $reward->id() . '|' . $reward_distance;
            if (!in_array($key, $completed_rewards)) {
              $available_rewards[$key] = $this->formatReward($username,
                $reward->label(),
                $reward_distance,
                $recurring);
            }
          }
        }
      }
    }
    return ($available_rewards);
  }

  /**
   * Helper function for getAvailableRewards(). Format into array.
   *
   * @param $username
   * @param $reward_label
   * @param $reward_distance
   * @param $recurring
   *
   * @return array
   */
  private function formatReward($username, $reward_label, $reward_distance, $recurring) {
    return [
      'username' => $username,
      'reward' => $reward_label,
      'distance' => $this->distanceMath->getDistance($reward_distance),
      'recurring' => ($recurring) ? $this->t('Yes') : $this->t('No'),
    ];
  }

  /**
   * Select total distance for each user from the database.
   *
   * @return mixed
   */
  private function getUserTotalMiles() {
    $query = $this->database->query("
      SELECT DISTINCT uid as `u.uid`, 
        (SELECT SUM(distance) 
        FROM employee_fitness_rewards_distance 
        where uid = `u.uid`) 
      AS distance
      FROM employee_fitness_rewards_distance
    ");
    return $query->fetchAllKeyed();
  }

}
