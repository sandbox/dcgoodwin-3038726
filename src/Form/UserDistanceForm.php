<?php

namespace Drupal\employee_fitness_rewards\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\employee_fitness_rewards\Math\distanceMath;

/**
 * Class UserDistanceForm.
 */
class UserDistanceForm extends FormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $fitnessRewardConfig;

  /**
   * Drupal\employee_fitness_rewards\Math\DistanceMath definition.
   *
   * @var \Drupal\employee_fitness_rewards\Math\DistanceMath
   */
  protected $distanceMath;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $rewardsEntityStorage;

  /**
   * Constructs a new UserDistanceForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    distanceMath $distance_math,
    AccountProxyInterface $current_user,
    Connection $database,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->fitnessRewardConfig = $config_factory->get('employee_fitness_rewards.config');
    $this->distanceMath = $distance_math;
    $this->currentUser = $current_user;
    $this->database = $database;
    try {
      $this->rewardsEntityStorage = $entity_type_manager->getStorage('fitness_reward_entity');
    } catch (\Exception $e) {
      $this->logger('employee_fitness_rewards')
        ->error('Error retrieving entity storage for fitness_reward_entity');
    }
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\Core\Form\FormBase|\Drupal\employee_fitness_rewards\Form\UserDistanceForm
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('employee_fitness_rewards.distance_math'),
      $container->get('current_user'),
      $container->get('database'),
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_distance_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL) {

    //Make sure user is admin or current user
    if ($this->checkAccess($user)->isForbidden()) {
      return $form['access_denied'] = [
        '#markup' => '<p>You do not have permission to access this form</p>',
      ];
    }

    $allowed_weeks = $this->distanceMath->getWeekTimestamps();
    $options = [];
    foreach ($allowed_weeks as $timestamp) {
      $options[$timestamp] = $this->t(date('m/d/Y', $timestamp));
    }

    $form['distance_form'] = [
      '#type' => 'fieldset',
      '#title' => 'Log Distance',
    ];
    $form['distance_form']['week_of'] = [
      '#type' => 'select',
      '#title' => $this->t('Week Of'),
      '#options' => $options,
      '#size' => 1,
      '#weight' => '0',
    ];
    $form['distance_form']['total_distance'] = [
      '#type' => 'number',
      '#min' => 0,
      '#max' => $this->fitnessRewardConfig->get('max_distance'),
      '#step' => 0.0001,
      '#title' => $this->t('Total Distance (@unit)',
        ['@unit' => $this->fitnessRewardConfig->get('unit_of_measurement')]),
      '#description' => $this->t('Total distance for week'),
      '#required' => TRUE,
      '#weight' => '1',
    ];

    $form['distance_form']['uid'] = [
      '#type' => 'hidden',
      '#value' => $user->id(),
    ];

    $form['distance_form']['log_distance'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => '2',
    ];

    $form['lifetime_distance'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Total Lifetime Distance'),
    ];

    $unit_short = ($this->fitnessRewardConfig->get('unit_of_measurement') == 'kilometers') ? 'km' : 'mi';
    $distance = $this->getTotalDistance($user);

    if(!empty($distance)) {
      $form['lifetime_distance']['distance'] = [
        '#type' => 'item',
        '#title' => $this->t('Total: @distance @unit',
          [
            '@distance' => $this->distanceMath->getDistance($distance->distance),
            '@unit' => $unit_short,
          ]),
      ];
    }

    $form['current_logs'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Current logs for this period'),
    ];

    $weeks = $this->getCurrentDistanceLogs($user, $allowed_weeks);

    if(!empty($weeks)) {
      foreach ($weeks as $week) {
        $form['current_logs'][$week->timestamp] = [
          '#type' => 'item',
          '#title' => $this->t('@timestamp - @distance @unit',
            [
              '@timestamp' => date('m/d/Y', $week->timestamp),
              '@distance' => $this->distanceMath->getDistance($week->distance),
              '@unit' => $unit_short,
            ]),
        ];
      }
    }

    $form['earned_rewards'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Rewards Received'),
    ];

    $rewards = $this->getUserRewards($user);

    if(!empty($rewards)) {
      foreach ($rewards as $reward) {
        $plural = ($reward->count == 1) ? 'time' : 'times';
        $form['earned_rewards'][$reward->label] = [
          '#type' => 'item',
          '#title' => $this->t('@label: Earned @num @plural',
            [
              '@label' => $reward->label,
              '@num' => $reward->count,
              '@plural' => $plural,
            ]),
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if ($form_state->getValue('total_distance') > $this->fitnessRewardConfig->get('max_distance')) {
      $form_state->setErrorByName('total_distance',
        $this->t('Total distance is above the weekly limit of @distance',
          ['@distance' => $this->fitnessRewardConfig->get('max_distance')]));
    }
  }

  /**
   * form submission handler
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // get submitted values
    $values = [
      'uid' => $this->currentUser->id(),
      'distance' => $this->distanceMath->getDistanceInsert($form_state->getValue('total_distance')),
      'timestamp' => $form_state->getValue('week_of'),
    ];
    // Check db for existing rewards for the selected week
    $select_existing = $this->database->select('employee_fitness_rewards_distance', 'td')
      ->fields('td', ['uid', 'timestamp'])
      ->condition('uid', $values['uid'])
      ->condition('timestamp', $values['timestamp'])
      ->execute()
      ->fetchAll();
    // Redirect to confirm form if data exists for selected week
    if (!empty($select_existing)) {
      $form_state->setRedirect('employee_fitness_rewards.overwrite_confirm',
        [
          'uid' => $form_state->getValue('uid'),
          'week_of' => $form_state->getValue('week_of'),
          'distance' => $this->distanceMath->getDistanceInsert($values['distance']),
        ]);
      return;
    }
    // Insert if data does not exist for selected week
    try {
      $success = $this->database->insert('employee_fitness_rewards_distance')
        ->fields($values)
        ->execute();
    } catch (\Exception $e) {
      $this->logger('Database exception occurred logging distance');
      $this->messenger()->addError($this->t('There was an error saving this record. 
     Please try again and contact the website administrator if this problem persists'));
    }
    if ($success) {
      $this->messenger()->addStatus($this->t('Distance logged successfully'));
    }

  }

  /**
   * @param \Drupal\user\UserInterface $user
   *
   * @return mixed
   */
  private function getTotalDistance(UserInterface $user) {
    $query = $this->database->query("
      SELECT DISTINCT uid, 
        (SELECT SUM(distance) 
        FROM employee_fitness_rewards_distance 
        WHERE uid = :uid) 
      AS distance
      FROM employee_fitness_rewards_distance
      WHERE uid = :uid
    ", [':uid' => $user->id()]);
    $result = $query->fetchObject();
    return $result;
  }

  /**
   * @param $user
   * @param $allowed_weeks
   *
   * @return mixed
   */
  private function getCurrentDistanceLogs(UserInterface $user, array $allowed_weeks) {
    $query = $this->database->select('employee_fitness_rewards_distance', 'td')
      ->fields('td', ['distance', 'timestamp'])
      ->orderBy('timestamp')
      ->condition('uid', $user->id());
    $group = $query->orConditionGroup();
    foreach ($allowed_weeks as $timestamp) {
      $group->condition('timestamp', $timestamp);
    }
    $results = $query->execute()->fetchAll();
    return $results;
  }

  /**
   * Get all existing awards for the user.
   * Recurring rewards are grouped by count.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return array
   */
  private function getUserRewards(UserInterface $user) {

    $entityRewards = $this->getRewardEntities();

    $query = $this->database->query("
      SELECT DISTINCT reward_id AS rid, 
        (SELECT COUNT(rid) 
        FROM employee_fitness_rewards_rewards 
        WHERE uid = :uid 
        AND reward_id = rid)
      AS total  
      FROM employee_fitness_rewards_rewards 
      WHERE uid = :uid
    ", [':uid' => $user->id()]);

    $results = $query->fetchAllKeyed();
    $rewards = [];
    if (!empty($results)) {
      foreach ($results as $key => $reward_count) {
        $label = $entityRewards[$key]->label();
        $reward_obj = new \stdClass();
        $reward_obj->label = $label;
        $reward_obj->count = $reward_count;
        $rewards[] = $reward_obj;
      }
    }
    return $rewards;
  }

  /**
   * Get all Fitness Reward config entities
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  private function getRewardEntities() {
    return $this->rewardsEntityStorage->loadMultiple();
  }

  /**
   * Returns access allowed if this page is for the current user or
   * if the current user is an admin.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  private function checkAccess(UserInterface $user) {
    $access = AccessResult::allowedIfHasPermission($this->currentUser,
      'access administration pages');
    if ($access->isAllowed()) {
      return $access;
    }
    return AccessResult::forbiddenIf($user->id() != $this->currentUser->id());
  }

}
