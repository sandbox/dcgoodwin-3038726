<?php

namespace Drupal\employee_fitness_rewards\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class DistanceConfirmOverwriteForm extends ConfirmFormBase {

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $id;

  /**
   * @var int
   */
  protected $uid;

  /**
   * Unix timestamp for the week to delete
   *
   * @var int
   */
  protected $weekOf;

  /**
   * @var number
   */
  protected $distance;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * ConfirmDeleteForm constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   * @param \Drupal\Core\Database\Connection $connection
   */
  public function __construct(AccountProxyInterface $current_user, Connection $connection) {
    $this->currentUser = $current_user;
    $this->connection = $connection;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\Core\Form\ConfirmFormBase|\Drupal\employee_fitness_rewards\Form\DistanceConfirmOverwriteForm
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    $uid = NULL,
    $week_of = NULL,
    $distance = NULL) {
    //Make sure user is admin or current user
    if ($this->checkAccess($uid)->isForbidden()) {
      return $form['access_denied'] = [
        '#markup' => '<p>You do not have permission to access this form</p>',
      ];
    }
    $this->uid = $uid;
    $this->weekOf = $week_of;
    $this->distance = $distance;
    return parent::buildForm($form, $form_state);
  }


  /**
   * Implementation of submitForm
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->connection->delete('employee_fitness_rewards_distance')
      ->condition('uid', $this->uid)
      ->condition('timestamp', $this->weekOf)
      ->execute();

    $this->connection->insert('employee_fitness_rewards_distance')
      ->fields(
        [
          'uid' => $this->uid,
          'distance' => $this->distance,
          'timestamp' => $this->weekOf,
        ])
      ->execute();

    $this->messenger()->addStatus($this->t('Overwrite was successful'));
    $form_state->setRedirect('entity.user.distance_form', ['user' => $this->uid]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "distance_confirm_overwrite_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.user.distance_form', ['user' => $this->uid]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Distance has already been logged for the week of
     %week_of. Do you want to overwrite existing data?',
      ['%week_of' => date('m/d/Y', $this->weekOf)]);
  }

  /**
   * Returns access allowed if this page is for the current user or
   * if the current user is an admin.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  private function checkAccess($uid) {
    $access = AccessResult::allowedIfHasPermission($this->currentUser,
      'access administration pages');
    if ($access->isAllowed()) {
      return $access;
    }
    return AccessResult::forbiddenIf($uid != $this->currentUser->id());
  }
}
