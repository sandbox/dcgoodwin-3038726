<?php

namespace Drupal\employee_fitness_rewards;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Provides a listing of Fitness Reward entities.
 */
class FitnessRewardEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * FitnessRewardEntityListBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage) {
    parent::__construct($entity_type, $storage);
    //This should ideally use dependency injection
    $this->config = \Drupal::service('config.factory')
      ->get('employee_fitness_rewards.config');
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    ;
    $header['label'] = $this->t('Fitness Reward');
    $header['distance'] = $this->t('Distance (@unit)', ['@unit' => $this->config->get('unit_of_measurement')]);
    $header['recurring'] = $this->t('Recurring');
    return $header + parent::buildHeader();
  }

  /**
   * @var $entity \Drupal\employee_fitness_rewards\Entity\FitnessRewardEntityInterface
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['distance'] = $entity->getDistance();
    $row['recurring'] = ($entity->isRecurring()) ? $this->t('Yes') : ($this->t('No'));
    return $row + parent::buildRow($entity);
  }
}
