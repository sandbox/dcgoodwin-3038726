<?php

namespace Drupal\employee_fitness_rewards\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\employee_fitness_rewards\Math\DistanceMath;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FitnessRewardEntityForm.
 */
class FitnessRewardEntityForm extends EntityForm {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\employee_fitness_rewards\Math\DistanceMath
   */
  protected $distanceMath;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * FitnessRewardEntityForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(ConfigFactoryInterface $configFactory, DistanceMath $distance_math, MessengerInterface $messenger) {
    $this->config = $configFactory->get('employee_fitness_rewards.config');
    $this->distanceMath = $distance_math;
    $this->messenger = $messenger;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\Core\Entity\EntityForm|\Drupal\employee_fitness_rewards\Form\FitnessRewardEntityForm
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('employee_fitness_rewards.distance_math'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /**
     * @var $fitness_reward_entity \Drupal\employee_fitness_rewards\Entity\FitnessRewardEntityInterface
     */
    $fitness_reward_entity = $this->entity;
    $unit = $this->config->get('unit_of_measurement');
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $fitness_reward_entity->label(),
      '#description' => $this->t("Label for the Fitness Reward."),
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $fitness_reward_entity->getDescription(),
      '#description' => $this->t("Description for the Fitness Reward."),
    ];

    $form['distance'] = [
      '#type' => 'number',
      '#title' => $this->t('Distance (@unit)', ['@unit' => $unit]),
      '#default_value' => $fitness_reward_entity->getDistance(),
      '#description' => $this->t("Distance in @unit required to earn the reward", ['@unit' => $unit]),
      '#required' => TRUE,
    ];

    $form['recurring'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Recurring'),
      '#default_value' => $fitness_reward_entity->isRecurring(),
      '#description' => $this->t("Is the Reward Recurring?"),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $fitness_reward_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\employee_fitness_rewards\Entity\FitnessRewardEntity::load',
      ],
      '#disabled' => !$fitness_reward_entity->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    /**
     * @var $fitness_reward_entity \Drupal\employee_fitness_rewards\Entity\FitnessRewardEntityInterface
     */
    $fitness_reward_entity = $this->entity;
    $fitness_reward_entity->setDistance($form_state->getValue('distance'));

    $status = $fitness_reward_entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger->addStatus($this->t('Created the %label Fitness Reward.', [
          '%label' => $fitness_reward_entity->label(),
        ]));
        break;

      default:
        $this->messenger->addStatus($this->t('Saved the %label Fitness Reward.', [
          '%label' => $fitness_reward_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($fitness_reward_entity->toUrl('collection'));
  }

}
