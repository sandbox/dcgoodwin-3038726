<?php

namespace Drupal\employee_fitness_rewards\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Employee Fitness Reward entities.
 */
interface FitnessRewardEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * @return string
   */
  public function getDescription();

  /**
   * @param string $description
   */
  public function setDescription($description);

  /**
   * @return float
   */
  public function getRawDistance();

  /**
   * @return float
   */
  public function getDistance();

  /**
   * @param float $distance
   */
  public function setDistance($distance);

  /**
   * @return bool
   */
  public function isRecurring();

  /**
   * @param bool $recurring
   */
  public function setRecurring($recurring);
}
