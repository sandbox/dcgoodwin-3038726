<?php

namespace Drupal\employee_fitness_rewards\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\employee_fitness_rewards\Math\DistanceMath;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TakeAHikeAdminForm.
 */
class FitnessRewardsAdminForm extends ConfigFormBase {

  /**
   * @var \Drupal\employee_fitness_rewards\Math\DistanceMath
   */
  protected $distanceMath;

  /**
   * TakeAHikeAdminForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\employee_fitness_rewards\Math\DistanceMath $distanceMath
   */
  public function __construct(ConfigFactoryInterface $config_factory, DistanceMath $distance_math) {
    parent::__construct($config_factory);
    $this->distanceMath = $distance_math;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'employee_fitness_rewards.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'employee_fitness_rewards_admin_form';
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\Core\Form\ConfigFormBase|\Drupal\employee_fitness_rewards\Form\FitnessRewardsAdminForm
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('employee_fitness_rewards.distance_math'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('employee_fitness_rewards.config');

    $form['#prefix'] = '<div id="ajax-wrapper">';
    $form['#suffix'] = '</div>';

    $form['unit_of_measurement'] = [
      '#type' => 'select',
      '#title' => $this->t('Unit of Measurement'),
      '#options' => [
        'kilometers' => $this->t('kilometers'),
        'miles' => $this->t('miles'),
      ],
      '#size' => 1,
      '#default_value' => $config->get('unit_of_measurement'),
      '#ajax' => [
        'callback' => '::updateDistanceUnit',
        'event' => 'change',
        'wrapper' => 'ajax-wrapper',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];
    $form['max_distance'] = [
      '#type' => 'number',
      '#min' => 0,
      '#step' => 0.0001,
      '#title' => $this->t('Max Distance (@unit)', ['@unit' => $config->get('unit_of_measurement')]),
      '#description' => $this->t('The maximum number of miles/kilometers a user can log in a single week'),
      '#default_value' => $this->distanceMath->getDistance($config->get('max_distance')),
    ];
    $form['allowed_weeks'] = [
      '#type' => 'number',
      '#min' => 1,
      '#step' => 1,
      '#title' => $this->t('Number of Weeks'),
      '#description' => $this->t('Number of previous weeks that a user is allowed to log data for.'),
      '#default_value' => $config->get('allowed_weeks'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * Update the value for max_distance whenever a change occurs on the
   * unit_of_measurement field
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function updateDistanceUnit(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('max_distance'))) {
      if ($form_state->getValue('unit_of_measurement') == 'miles') {
        $form['max_distance']['#value'] = $this->distanceMath
          ->convertToMiles($form_state->getValue('max_distance'));
        $form['max_distance']['#title'] = $this->t('Max Distance (miles)');
      }
      else {
        $form['max_distance']['#value'] = $this->distanceMath
          ->convertToKilometers($form_state->getValue('max_distance'));
        $form['max_distance']['#title'] = $this->t('Max Distance (kilometers)');
      }
    }
    $form_state->setRebuild();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    //set unit of measurement first so that max_distance is calculated correctly
    $this->config('employee_fitness_rewards.config')
      ->set('unit_of_measurement', $form_state->getValue('unit_of_measurement'))
      ->save();
    //set other values only after unit_of_measurement is set
    $this->config('employee_fitness_rewards.config')
      ->set('max_distance', $this->distanceMath
        ->getDistanceInsert($form_state->getValue('max_distance')))
      ->set('allowed_weeks', $form_state->getValue('allowed_weeks'))
      ->save();
  }

}
