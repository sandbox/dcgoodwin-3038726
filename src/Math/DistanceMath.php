<?php

namespace Drupal\employee_fitness_rewards\Math;


use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Utility service for fitness rewards module. This class is primarily used to
 * convert kilometers and miles but is also used to calculate date values.
 *
 * Class DistanceMath
 *
 * @package Drupal\employee_fitness_rewards\Math
 */
class DistanceMath {

  /**
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   *
   * DistanceMath constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
    $this->config = $this->configFactory->get('employee_fitness_rewards.config');
  }

  /**
   * Return the value of Distance.
   * Convert to miles if set
   *
   * @param $distance
   *
   * @return float
   */
  public function getDistance($distance) {
    if ($this->config->get('unit_of_measurement') == 'kilometers') {
      return (float) $distance;
    }
    else {
      return $this->convertToMiles($distance);
    }
  }

  /**
   * Return the value of distance that should be set when saving.
   * convert to kilometers if config is set to miles
   *
   * @param $distance
   *
   * @return float
   */
  public function getDistanceInsert($distance) {
    if ($this->config->get('unit_of_measurement') == 'kilometers') {
      return (float) $distance;
    }
    else {
      return $this->convertToKilometers($distance);
    }
  }

  /**
   * @param $miles
   *
   * @return float
   */
  public function convertToKilometers($miles) {
    return round(((float) $miles) * 1.609344, 4);
  }

  /**
   * @param $kilometers
   *
   * @return float
   */
  public function convertToMiles($kilometers) {
    return round(((float) $kilometers) * 0.621371, 4);
  }

  /**
   * Use the admin config form settings to generate timestamps for allowed_weeks
   *
   * @return array
   */
  public function getWeekTimestamps() {
    $allowed_weeks = $this->config->get('allowed_weeks');
    $timestamps = [];
    $offset = 0;
    $index = 0;
    while ($index < $allowed_weeks) {
      $timestamps[] = mktime(0, 0, 0, date("n"), date("j") - date("N") - $offset);
      $offset += 7;
      $index++;
    }
    return $timestamps;
  }
}