<?php

namespace Drupal\employee_fitness_rewards\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;


/**
 * Class UserDeleteRewardsForm.
 */
class UserAdminDeleteRewardsForm extends UserAdminFormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_delete_rewards_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $header = [
      'username' => $this->t('Username'),
      'reward' => $this->t('Reward Title'),
      'distance' => $this->t('Distance (@unit)',
        ['@unit' => $this->fitnessRewardsConfig->get('unit_of_measurement')]),
      'recurring' => $this->t('Recurring'),
    ];

    $options = $this->getOptions();

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => $this->t('No records to display.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // get values and explode key into uid, reward machine_name and distance
    $values = $form_state->getValue('table');
    $delete = [];
    foreach ($values as $reward) {
      if (!empty($reward[0])) {
        $delete[] = explode('|', $reward);
      }
    }
    $total_results = 0;
    foreach ($delete as $delete_reward) {
      $results = $this->database->delete('employee_fitness_rewards_rewards')
        ->condition('uid', $delete_reward[0])
        ->condition('reward_id', $delete_reward[1])
        ->condition('distance', $delete_reward[2])
        ->execute();
      $total_results += (int) $results;
    }
    if ($total_results) {
      $plural = ($total_results > 1) ? 'records' : 'record';
      $this->messenger()->addStatus($this->t(
        'Successfully deleted %num @plural',
        ['%num' => $total_results, '@plural' => $plural])
      );
    }
    else {
      $this->messenger()->addWarning($this->t('No records deleted.'));
    }
  }

  /**
   * Get rows for table form.
   *
   * @return array
   */
  private function getOptions() {

    $rewards = $this->getRewardEntities();
    $completed_rewards = $this->getCompletedUserRewards();

    $options = [];
    foreach ($completed_rewards as $key) {

      // Explode key into uid, reward machine_name, and distance.
      $parts = explode('|', $key);

      // Get get username for uid.
      $user = User::load($parts[0]);
      $username = $user->getUsername();

      // Get reward name and recurring boolean
      /**
       * @var $reward \Drupal\employee_fitness_rewards\Entity\FitnessRewardEntityInterface
       */
      $reward = $rewards[$parts[1]];
      $reward_name = $reward->label();
      $recurring = $reward->isRecurring();
      $recurring = ($recurring) ? 'Yes' : 'No';
      $options[$key] = [
        'username' => $username,
        'reward' => $reward_name,
        'distance' => $this->distanceMath->getDistance($parts[2]),
        'recurring' => $recurring,
      ];
    }
    return $options;
  }

}
